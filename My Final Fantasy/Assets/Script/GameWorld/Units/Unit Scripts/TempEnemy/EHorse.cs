﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EHorse : NPCMove {

    public EHorse() {
        move = 6;
        moveSpeed = 4;
        maxMove = move;

        attackPoints = 9;
        defensePoints = 8;
        damagePoints = 7;

        healthPoints = 15;

        attackRange = 2;
        isMelee = true;

        //ability = SpecialAbility.getAbility();
    }
}
