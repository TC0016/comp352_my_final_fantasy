﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EArcher : NPCMove {

	public EArcher() {
        move = 3;
        moveSpeed = 2;
        maxMove = move;
        /* Base Stats
        damagePoints = 0;
        attackPoints = 1;
        defensePoints = 5;
        */
		
        //Special Stats
        attackPoints = 8;
        defensePoints = 5;
		damagePoints = 4;

        healthPoints = 10;
		
		attackRange = 7;
        isRange = true;
    }
}
