﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : Unit {

    public Archer() {
        move = 3;
        moveSpeed = 2;
        maxMove = move;
        /* Base Stats
        damagePoints = 0;
        attackPoints = 1;
        defensePoints = 5;
        */
		
        //Special Stats
        damagePoints = 4;
        attackPoints = 8;
        defensePoints = 5;

        healthPoints = 10;
		
		attackRange = 7;
        isRange = true;
    }
}
