﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TacticsMove : MonoBehaviour {

    public bool turn = false;

    List<Tile> selectableTiles = new List<Tile>();
    GameObject[] tiles;

    Stack<Tile> path = new Stack<Tile>();
    Tile currentTile;

    public bool moving = false;

	
	//Could moves these elsewhere
	public SpecialAbility ability;
	public List<SpecialAbility> abilities = new List<SpecialAbility>();
    //public List<SpecialAbilities> abilities;

    protected CombatManager combatManager = new CombatManager();
	
	protected CombatManager combatManager = new CombatManager();
	
    public string unitName;
    protected int damagePoints;
    protected int attackPoints;
    protected int defensePoints;
    public int healthPoints;
    public int move;
    public int maxMove;
    public float moveSpeed;
    public bool isPlayer;

    //Temporary (Will me Implemented with Special Abilities)
    public bool attackActive = false;
	public int attackRange;
    public bool isMelee = false;
    public bool isSpear = false;
    public bool isRange = false;

    private bool initPathCount = true;
    private int currentMoved;

    Vector3 velocity = new Vector3();
    Vector3 heading = new Vector3();

    public AudioClip moveSound;
    public AudioClip hitSound;
    public AudioClip missSound;

    public AudioClip selectSound;
    public AudioClip deathSound;
	
	private AudioSource hitSource;


    //float halfHeight = 0;

    public Tile actualTargetTile;

    private void Start()
    {
        GetCurrentTile();
    }

    protected void Init() {
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        //halfHeight = GetComponent<Collider>().bounds.extents.y;

        TurnManager.AddUnit(this);
    }

    public void GetCurrentTile() {
        currentTile = GetTargetTile(gameObject);
        currentTile.current = true;
        currentTile.unit = this;
    }

    public Tile GetTargetTile(GameObject target) {
        RaycastHit hit;
        Tile tile = null;
        
        if (Physics.Raycast(target.transform.position, -Vector3.up, out hit, 1)){
            tile = hit.collider.GetComponent<Tile>();
        }

        return tile;
    }

    public void computeAdjacencyLists(Tile target) {
        //tiles = GameObject.FindGameObjectsWithTag("Tile");
        
        foreach(GameObject tile in tiles) {
            Tile t = tile.GetComponent<Tile>();
            t.findNeighbors(target, attackActive);
        }
    }

    public void FindSelectableTiles() {
        computeAdjacencyLists(null);
        GetCurrentTile();
        
        Queue<Tile> process = new Queue<Tile>();

        process.Enqueue(currentTile);
        currentTile.visited = true;
        //currentTile.parent = ??
		if(!attackActive){
			while(process.Count > 0) {
				Tile t = process.Dequeue();

				selectableTiles.Add(t);
				t.selectable = true;

				if (t.walkable == false) {
					t.selectable = false;
				}

				if (t.distance < move) {
					foreach (Tile tile in t.adjacencyList) {
						if (!tile.visited && tile.walkable) {
							tile.parent = t;
							tile.visited = true;
							tile.distance = 1 + t.distance;
							process.Enqueue(tile);
						}
					}
				}
			} 
		} else if (attackActive) {
            FindAttackTile();
        }
    }
	
	public void removeSelectableTiles(){
		computeAdjacencyLists(null);
		return;
	}

    public void MoveToTile(Tile tile) {
        path.Clear();
        tile.target = true;
        moving = true;

        Tile next = tile;

        while(next != null) {
            path.Push(next);
            next = next.parent;
        }
    }

    public void Move() {
        currentTile.unit = null;
        if (path.Count > 0) {
            Tile t = path.Peek();
            Vector3 target = t.transform.position;
            
            if (initPathCount) {
                move = move + 1 - path.Count;
                initPathCount = false; 
            }

            //MAY NOT NED
            //Calculate the unit's position on top of the target tile
            //target.y += halfHeight + t.GetComponent<Collider>().bounds.extents.y;
            target.y += 1;

            if (Vector3.Distance(transform.position, target) >= 0.05f) {
                CalculateHeading(target);
                SetHorizontalVelocity();

                transform.forward = heading;
                transform.position += velocity * Time.deltaTime;                
            } else {
                //Tile center reached
                AudioSource.PlayClipAtPoint(moveSound, Camera.main.transform.position);

                currentTile = t;
                t.walkable = true;
                transform.position = target;
                path.Pop();
            }
        } else {
            currentTile.unit = this;
            currentTile.walkable = false;

            RemoveSelectableTiles();
            moving = false;
            initPathCount = true;

            /* For Testing Purposes it is off to speed up testing (It does work) */
            if (move <= 0) {
                TurnManager.EndTurn();
            }
        }
    }

    protected void RemoveSelectableTiles() {
        if(currentTile != null) {
            currentTile.current = false;
            currentTile = null;
        }

        foreach(Tile tile in selectableTiles) {
            tile.Reset();
        }

        selectableTiles.Clear();
    }

    void CalculateHeading(Vector3 target) {
        heading = target - transform.position;
        heading.Normalize();
    }

    void SetHorizontalVelocity() {
        velocity = heading * moveSpeed;
    }

    protected Tile FindLowestF(List<Tile> list) {
        Tile lowest = list[0];

        foreach(Tile t in list) {
            if(t.f < lowest.f) {
                lowest = t;
            }
        }

        list.Remove(lowest);

        return lowest;
    }
    //TODO COMBAT CHANGES
    protected Tile FindEndTile(Tile t) {
        Stack<Tile> tempPath = new Stack<Tile>();

        Tile next = t.parent;
        while(next != null) {
            tempPath.Push(next);
            next = next.parent;
        }

        if (tempPath.Count <= move) {
            return t.parent;
        }

        Tile endTile = null;
        for(int i = 0; i <= move; i++) {
            endTile = tempPath.Pop();
        }

        return endTile;
    }


    protected void FindPath(Tile target) {
        computeAdjacencyLists(target);
        GetCurrentTile();

        List<Tile> openList = new List<Tile>();
        List<Tile> closedList = new List<Tile>();

        //TODO Priority Queue? for Openlist
        openList.Add(currentTile);
        //currentTIle.parent = ??

        currentTile.h = Vector3.Distance(currentTile.transform.position, target.transform.position);
        currentTile.f = currentTile.h;

        while(openList.Count > 0) {
            Tile t = FindLowestF(openList);

            closedList.Add(t);

            if(t == target) {
                actualTargetTile = FindEndTile(t);
                MoveToTile(actualTargetTile);
                return;
            }
            
            foreach (Tile tile in t.adjacencyList) {
                if (closedList.Contains(tile)) {                    
                    //Do nothing, allready processed
                } else if (openList.Contains(tile)) {
                    //If a faster path is found
                    float tempG = t.g + Vector3.Distance(tile.transform.position, t.transform.position);

                    if(tempG < t.g) {
                        tile.parent = t;
                        tile.g = tempG;
                        tile.f = tile.g + tile.h;
                    }
                } else {
                    tile.parent = t;

                    tile.g = t.g + Vector3.Distance(tile.transform.position, t.transform.position);
                    tile.h = Vector3.Distance(tile.transform.position, target.transform.position);
                    tile.f = tile.g + tile.h;
                    openList.Add(tile);
                }
            }
        }

        //Todo - What if there is no path for the target tile?
        Debug.Log("Target not Found");
    }

    public void BeginTurn() {
        //AudioSource.PlayClipAtPoint(selectSound, Camera.main.transform.position);
        turn = true;
    }

    public void EndTurn() {
        turn = false;
    }

    public void FindAttackTile() {
        Tile t = GetTargetTile(gameObject);
        
        if (move > 0) {
            searchForEnemies(attackRange, isPlayer);
			//Melee
			//searchForEnemies(1);
			
			//Spear
			//searchForEnemies(2, isPlayer);
			
			//Ranged
			//searchForEnemies(7);
			
            //MeleeTile(t);
            /*
            RangedTile();
            */
        }
    }

    public void searchForEnemies(int attackRange, bool isPlayerTeam) {
        Queue<Tile> process = new Queue<Tile>();

        process.Enqueue(currentTile);
        currentTile.visited = true;
        //currentTile.parent = ??
		
        while (process.Count > 0) {
            Tile t = process.Dequeue();
			
			if (t.distance < attackRange) {
                foreach (Tile tile in t.adjacencyList) {
					if(!tile.visited){
						tile.parent = t;
						tile.visited = true;
						tile.distance = 1 + t.distance;
						
						tile.attackRange = true;
						
						try {
							if (tile.unit.isPlayer != isPlayerTeam) {
								tile.attackable = true;
							}
						} catch {
						}
						
						process.Enqueue(tile);
					}
                }
            }
        }
    }
<<<<<<< HEAD
	
	public int getAttackPoints(){
		return attackPoints;
	}
	
	public int getDefensePoints(){
		return defensePoints;
	}
	
	public int getDamagePoints(){
		return damagePoints;
	}
	
	public AudioClip getSound(string attackSound){
		if(attackSound == "Hit"){
			return hitSound;
		}
		
		return missSound;
	}
=======

    public int getDamagePoints() {
        return damagePoints;
    }

    public int getAttackPoints() {
        return attackPoints;
    }

    public int getDefensePoints() {
        return defensePoints;
    }

    public AudioClip getSound(string check) {
        if(check == "Hit") {
            return hitSound;
        } else if (check == "Miss") {
            return missSound;
        }
        return null;
    }
>>>>>>> 60e3143437d4ba74c5fd7761090fd4437b5a9b9a
}
