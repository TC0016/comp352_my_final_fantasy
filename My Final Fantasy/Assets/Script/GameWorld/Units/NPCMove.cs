﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//https://www.youtube.com/watch?v=3M3FcJ4r0GE&t=215s&ab_channel=GameProgrammingAcademy

public class NPCMove : TacticsMove {

    GameObject target;

	void Start () {
        Init();

        GetCurrentTile();
    }
    void Update() {
        if (!turn) {
            return;
        }
        
        if (!moving) {
            FindNearestTarget();
            CalculatePath();
            FindSelectableTiles();
            actualTargetTile.target = true;

            Attack();
            move = 0;
        } else {
            Move();
        }
    }

    void Attack() {
        Tile targetTile = GetTargetTile(target);
        attackActive = true;
        computeAdjacencyLists(null);
        FindAttackTile();

        if (targetTile.attackable) {
<<<<<<< HEAD
            Debug.Log("HELLO");
            combatManager.Combat(targetTile);
=======
            combatManager.mainCombat(targetTile);
>>>>>>> 60e3143437d4ba74c5fd7761090fd4437b5a9b9a
        }

        attackActive = false;
    }

    void CalculatePath() {
        Tile targetTile = GetTargetTile(target);
        
        FindPath(targetTile);
    }

    void FindNearestTarget() {
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Player");

        GameObject nearest = null;
        float distance = Mathf.Infinity;

        foreach(GameObject obj in targets) {
            float d = Vector3.Distance(transform.position, obj.transform.position);

            if(d < distance) {
                distance = d;
                nearest = obj;
            }
        }
        target = nearest;
    }
}
