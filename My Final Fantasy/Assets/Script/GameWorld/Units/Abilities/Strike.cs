﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strike : SpecialAbility {

	public Strike(){
		name = "Strike";
		isMelee = true;
		attackRange = 1;
	}
}
