﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class AttackButton : MonoBehaviour {

    TacticsMove unit = new TacticsMove();
    TurnManager currentUnit = new TurnManager();

    Tile tile = new Tile();

    //public AudioSource clickButton;
    public AudioClip clickButton;

    public void PointerClick() {
        if(currentUnit.getSelectedUnit() == null) {
            return;
        }
        unit = currentUnit.getSelectedUnit();

        if (unit.move <= 0) {
            return;
        }

        if (!unit.attackActive) {
            unit.attackActive = true;
        } else {
            unit.attackActive = false;
        }
        
        AudioSource.PlayClipAtPoint(clickButton, Camera.main.transform.position);
    }
}
