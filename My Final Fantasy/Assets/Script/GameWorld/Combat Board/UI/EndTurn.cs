﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTurn : MonoBehaviour {

    public AudioClip clickButton;

    public void PointerClick() {
        AudioSource.PlayClipAtPoint(clickButton, Camera.main.transform.position);
        TurnManager.properEndTurn();
    }
}
