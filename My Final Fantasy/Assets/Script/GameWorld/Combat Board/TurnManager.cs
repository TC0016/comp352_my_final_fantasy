﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//https://docs.google.com/forms/d/1A2Y-nxKtuAUN6QJbcOAqC4f_bw-lHfIJ69o0-f8rTB0/edit
public class TurnManager : MonoBehaviour {
    static Queue<string> turnKey = new Queue<string>();
    static Queue<TacticsMove> turnTeam = new Queue<TacticsMove>();

    //static Dictionary<string, List<TacticsMove>> units = new Dictionary<string, List<TacticsMove>>();
    static List<TacticsMove> units = new List<TacticsMove>();
    static List<TacticsMove> playerUnits = new List<TacticsMove>();
    static List<TacticsMove> enemyUnits = new List<TacticsMove>();
    //static Dictionary<string, List<TacticsMove>> playerUnits = new Dictionary<string, List<TacticsMove>>();
    //static Dictionary<string, List<TacticsMove>> enemyUnits = new Dictionary<string, List<TacticsMove>>();

    public static bool playerTurn;

    private Tile currentTile;

    public TacticsMove[,] Units { set; get; }
    private static TacticsMove selectedUnit;

    private int selectionX = -1;
    private int selectionY = -1;
	
	private static int currentEnemyUnit;
	
	private static bool isPlayerTurn = true;


    public TacticsMove getSelectedUnit() {
        return selectedUnit;
    }
    // Use this for initialization
    void Start () {
		
	}

    public void StartTurn() {
        if (currentTile.unit.isPlayer != isPlayerTurn) {
            return;
        }

        selectedUnit.BeginTurn();
        /* else if(turnTeam.Count = 0) { Checks if a team is dead
         * 
         */
    }

    // Update is called once per frame
    void Update () {
        UpdateSelection();

        if (Input.GetMouseButtonDown(0)) {
            if (currentTile != null) {
				if (selectedUnit == null && currentTile.unit != null) {
					//Select the Chessman
					SelectUnit(selectionX, selectionY);
					StartTurn();
				} else if(selectedUnit != null) {
					deselectUnit();
				}
            }
            /*Allow to deselect unit when LClick anywhere*/
        }
        /*
        if (turnTeam.Count == 0) {
            InitTeamTurnQueue();
        }*/
	}

    private void SelectUnit(int x, int y) {
		
		if(currentTile.unit.isPlayer != isPlayerTurn) {
			return;
		}
			
        selectedUnit = currentTile.unit;

        //TODO
        /*
        if (Units[x, y].isPlayer != isWhiteTurn)
            return;
            
        //allowedMoves = Units[x, y].PossibleMove();

        selectedUnit = Units[x, y];

        previousMat = selectedChessman.GetComponent<MeshRenderer>().material;
        selectedMat.mainTexture = previousMat.mainTexture;
        selectedChessman.GetComponent<MeshRenderer>().material = selectedMat;
        */
    }
	
	private void deselectUnit() {
        if (selectedUnit.moving) {
            return;
        }

        selectedUnit.EndTurn();
		selectedUnit.removeSelectableTiles();
		selectedUnit = null;
	}

    public static void EndTurn() {        
        selectedUnit.EndTurn();
        selectedUnit = null;
		/*
		if(!isPlayer){
			for(int i = 0; i < enemyUnits.Count; i++){
				if(enemyUnits[i].move > 0){
					enemyUnits[i].turn = true;
					selectedUnit = enemyUnits[i];
				}
			}
		}
		*/
		/*
        if (isPlayer) {
            if (teamEnd(playerUnits)) {
                enemyUnits[0].turn = true;
                //Debug.Log(enemyUnits[0].unitName);
                selectedUnit = enemyUnits[0];
            }
        } else if(!isPlayer) {
            //Debug.Log("HELLO");
            teamEnd(enemyUnits);
			NPCEndTurn();
        }
		*/
		if(!isPlayerTurn) {
            //Debug.Log("HELLO");
            teamEnd(enemyUnits);
			NPCEndTurn();
        }
		/*
        for(int i = 0; i < units.Count; i++) {
            if (units[i].move <= 0) {
                if (i == units.Count-1) {
                    roundEnd();
                }
            } else {
                break;
            }
        }
		*/
    }

    private void UpdateSelection() {

        if (!Camera.main)
            return;

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("CombatPlane")))
        {
            //Debug.Log("HELLO THERE");
            currentTile = hit.collider.gameObject.GetComponent<Tile>();
            //Debug.Log(currentTile.unit);
            //selectionX = (int)hit.point.x;
            //selectionY = (int)hit.point.z;
        }
        else
        {
            //Debug.Log("GENERAL KENOBI");
            currentTile = null;
            //selectionX = -1;
            //selectionY = -1;
        }
    }

    private static bool teamEnd(List <TacticsMove> team) {
        for(int i = 0; i < team.Count; i++) {
            if(team[i].move <= 0) {
                if (i == team.Count-1) {
                    isPlayerTurn = !isPlayerTurn;
                    return true;
                }
            } else {
                return false;
            }
        }

        return false;
    }

    private static void roundEnd() {
        for (int i = 0; i < units.Count; i++) {
            units[i].move = units[i].maxMove;
        }
    }
	
	public static void properEndTurn(){
		if(!isPlayerTurn) {
			return;
		}

		if(selectedUnit != null){
			selectedUnit.EndTurn();
			selectedUnit.removeSelectableTiles();
			selectedUnit = null;
		}

        isPlayerTurn = !isPlayerTurn;
		currentEnemyUnit = -1;
		
		roundEnd();
		NPCEndTurn();
	}
	
	public static void NPCEndTurn(){
		//Bug: Argument is out of range
		currentEnemyUnit++;
		if(currentEnemyUnit > enemyUnits.Count){
			return;
		}
		
		selectedUnit = enemyUnits[currentEnemyUnit];
		selectedUnit.turn = true;
	}

    public static void AddUnit(TacticsMove unit) {
        if (unit.isPlayer) {
            playerUnits.Add(unit);
        } else {
            enemyUnits.Add(unit);
        }
        units.Add(unit);
    }

    public static void RemoveUnit(TacticsMove unit) {
        units.Remove(unit);
    }

    /*
    private void MoveUnit(int x, int y)
    {
        bool combatOccured = false;

        if (allowedMoves[x, y])
        {
            Chessman c = Chessmans[x, y];
            if (c != null && c.isWhite != isWhiteTurn)
            {
                //Capture a piece

                //If it is the king
                if (c.GetType() == typeof(King))
                {
                    EndGame();
                    return;
                }
                combatOccured = mainCombat(selectedChessman, c);
            }

            if (!combatOccured)
            {
                Chessmans[selectedChessman.CurrentX, selectedChessman.CurrentY] = null;
                selectedChessman.transform.position = GetTileCenter(x, y);
                selectedChessman.SetPosition(x, y);
                Chessmans[x, y] = selectedChessman;

                selectedChessman.setMovePoints(selectedChessman.getMovePoints() - 1);
            }

            if (selectedChessman.getMovePoints() <= 0)
            {
                selectedChessman.setMovePoints(selectedChessman.getMaxMovePoints());
                combatOccured = !combatOccured;
                isWhiteTurn = !isWhiteTurn;
            }
        }

        selectedChessman.GetComponent<MeshRenderer>().material = previousMat;
        BoardHighlights.Instance.HideHighLights();

        selectedChessman = null;
    }
    */
    /*
    static void InitTeamTurnQueue() {
        List<TacticsMove> teamList = units[turnKey.Peek()];

        foreach(TacticsMove unit in teamList) {
            turnTeam.Enqueue(unit);
        }

        StartTurn();
    }

    public static void StartTurn() {
        if(turnTeam.Count > 0) {
            turnTeam.Peek().BeginTurn();
        }
        /* else if(turnTeam.Count = 0) { Checks if a team is dead
         * 
         */
    /*    }

        public static void EndTurn() {
            TacticsMove unit = turnTeam.Dequeue();
            unit.EndTurn();

            if(turnTeam.Count > 0) {
                StartTurn();
            } else {
                string team = turnKey.Dequeue();
                turnKey.Enqueue(team);
                InitTeamTurnQueue();
            }
        }

        public static void AddUnit(TacticsMove unit) {
            List<TacticsMove> list;

            if (!units.ContainsKey(unit.tag)) {
                list = new List<TacticsMove>();
                units[unit.tag] = list;

                if (!turnKey.Contains(unit.tag)) {
                    turnKey.Enqueue(unit.tag);
                }
            } else {
                list = units[unit.tag];
            }

            list.Add(unit);
        }
        /* Remove unit from list
         * 
         * 
         * 
         */
}
